#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: CrS-CTL:Vac-VEG-070
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = CrS-CTL:Vac-VEG-070, IPADDR = crs-ctl-moxa.tn.esss.lu.se, PORT = 4001")

#
# Device: CrS-CTL:Cryo-PT-895
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-895, CHANNEL = A1, CONTROLLERNAME = CrS-CTL:Vac-VEG-070")

#
# Device: CrS-CTL:Cryo-PT-893
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-893, CHANNEL = B1, CONTROLLERNAME = CrS-CTL:Vac-VEG-070")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-893, RELAY = 1, RELAY_DESC = 'High Vacuum '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-893, RELAY = 2, RELAY_DESC = 'Not used '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-893, RELAY = 3, RELAY_DESC = ' '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-893, RELAY = 4, RELAY_DESC = ' '")

#
# Device: CrS-CTL:Cryo-PT-892
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-892, CHANNEL = A2, CONTROLLERNAME = CrS-CTL:Vac-VEG-070")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-892, RELAY = 1, RELAY_DESC = 'Rough Vacuum'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = CrS-CTL:Cryo-PT-892, RELAY = 2, RELAY_DESC = 'Not used'")
