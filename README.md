# IOC for CTL Box vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   CrS-CTL:Vac-VEG-070
    *   CrS-CTL:Cryo-PT-892
    *   CrS-CTL:Cryo-PT-893
    *   CrS-CTL:Cryo-PT-895
